<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RouteController extends Controller
{
    public function superadmin()
    {
        return 'ini adalah route superadmin';
    }
    
    public function admin()
    {
        return 'ini adalah route admin';
    }
    
    public function guest()
    {
        return 'ini adalah route guest';
    }
}
