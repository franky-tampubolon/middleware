<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use Illuminate\Support\Facades\Auth;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // cek uri
        if($request->route()->uri()=='superadmin')
        {
            // cek role_id
            if(Auth::user()->isRole() =='superadmin'){
                return $next($request);
            }else{
                abort(403);
            }
        }
        elseif($request->route()->uri()=='admin')
        {
            if(Auth::user()->isRole() == 'admin' || Auth::user()->isRole() == 'superadmin')
            {
                return $next($request);
            }else{
                abort(403);
            }
        }
        else{
            return $next($request);
        }
        // ijinkan uri guest untuk semua role_id
        // ijinkan uri admin untuk role_id admin dan superadmin
        // ijinkan uri superadmin untuk role_id superadmin
        

    }
}
